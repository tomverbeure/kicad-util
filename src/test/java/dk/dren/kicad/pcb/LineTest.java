package dk.dren.kicad.pcb;

import dk.dren.kicad.primitive.Point;
import org.decimal4j.immutable.Decimal6f;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class LineTest {

    @Test
    public void parallel() {
        Line a = new Line(new Point(0, 0), new Point(10, 10));
        Line b = new Line(new Point(1, 1), new Point(11, 11));

        Assert.assertNull(a.intersection(b));
    }

    @Test
    public void hit() {
        //Line(start=Point(x=58.000000, y=85.999995), end=Point(x=55.000000, y=85.999995))
        //Line(start=Point(x=58.000000, y=76.000005), end=Point(x=55.000000, y=76.000005))
        //Line(start=Point(x=56.050000, y=89.800000), end=Point(x=56.050000, y=53.800000))
        Line a = new Line(new Point(0, 0), new Point(10, 10));
        Line b = new Line(new Point(0, 10), new Point(10, 0));

        Point intersection = a.intersection(b);
        Assert.assertNotNull(intersection);

        Assert.assertEquals(intersection, new Point(5, 5));
    }

    @Test
    public void failIt() {
        //Line(start=Point(x=58.000000, y=85.999995), end=Point(x=55.000000, y=85.999995))
        //Line(start=Point(x=58.000000, y=76.000005), end=Point(x=55.000000, y=76.000005))
        //Line(start=Point(x=56.050000, y=89.800000), end=Point(x=56.050000, y=53.800000))
        Line a = new Line(new Point(56.050000, 89.800000), new Point(56.050000, 53.800000));
        Line b = new Line(new Point(58.000000,76.000005), new Point(55.000000, 76.000005));

        Point intersection = a.intersection(b);
        Assert.assertNotNull(intersection);
    }

    @Test
    public void noCross() {

        Line a = new Line(new Point(50, 50), new Point(60, 60));
        Line b = new Line(new Point(0, 10), new Point(10, 0));
        Assert.assertNull(a.intersection(b));
    }

    @Test
    public void circleIntersection() {

        Line line = new Line(new Point(10, 10), new Point(10, 20));

        List<Point> intersections = line.intersection(new Point(12, 25), Decimal6f.valueOf(10));

        Assert.assertEquals(1, intersections.size());
        Point i = intersections.get(0);

        Assert.assertEquals(Decimal6f.valueOf(10), i.getX());
        Assert.assertEquals(15.2, i.getY().doubleValue(), 0.01);

    }

}