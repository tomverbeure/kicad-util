package dk.dren.kicad.pcb;

import dk.dren.kicad.primitive.Point;
import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;

@Getter
public class Polygon implements Node {
    public static final String POLYGON = "polygon";
    public static final String FILLED_POLYGON = "filled_polygon";

    private final String name;
    private final List<Point> points;

    public Polygon(RawNode node) {
        name = node.getName();
        points = node.getFirstChildByName("pts").get().streamChildNodesByName("xy")
                .map(this::nodeToPoint)
                .collect(Collectors.toList());
    }

    public boolean contains(Point test) {
        return contains(test.getX().unscaledValue(), test.getY().unscaledValue());
    }

    public boolean contains(Module m) {
        return contains(m.getPosition().getXyr());
    }

    public boolean containsNode(Node node, String property) {
        Optional<Node> maybeAt = node.getFirstChildByName(property);
        if (!maybeAt.isPresent()) {
            throw new IllegalArgumentException("The node "+node+" doesn't have a child called "+property);
        }
        Node at = maybeAt.get();
        if (at instanceof Position) {
            return contains(((Position) at).getXyr());
        } else {
            throw new IllegalArgumentException("The node "+node+" has the child "+ at +" but it's not a Position");
        }
    }

    private boolean contains(long tx, long ty) {

        boolean result = false;
        int i;
        int j;
        for (i = 0, j = points.size()-1; i < points.size(); j = i++) {
            final Point pi = points.get(i);
            final Point pj = points.get(j);

            long pix = pi.getX().unscaledValue();
            long piy = pi.getY().unscaledValue();
            long pjx = pj.getX().unscaledValue();
            long pjy = pj.getY().unscaledValue();

            if ((piy > ty) != (pjy > ty) &&
                    (tx < (pjx - pix) * (ty - piy) / (pjy-piy) + pix)) {
                result = !result;
            }
        }

        return result;
    }

    private Point nodeToPoint(Node n) {
        List<MutableValue> attributes = n.getAttributes();
        if (n.getAllChildren().size() != 2 || attributes.size() != 2) {
            throw new IllegalArgumentException("Need exactly two attributes as children, found: "+n);
        }

        return new Point(attributes.get(0).getDecimal6f(), attributes.get(1).getDecimal6f());
    }

    private NodeOrValue pointToNode(Point point) {
        List<NodeOrValue> attr = new ArrayList<>();
        attr.add(new MutableValue(point.getX().toString()));
        attr.add(new MutableValue(point.getY().toString()));
        return new RawNode("xy", attr);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Collection<NodeOrValue> getAllChildren() {
        return Collections.singletonList(new RawNode("pts",
                points.stream()
                        .map(this::pointToNode)
                        .collect(Collectors.toList()))
        );
    }

}
